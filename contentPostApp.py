#!/usr/bin/python3

"""
contentPostAPp -> Ejercicio de Antonio Perona Martinez
"""

import webapp


class ContentPostApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""
        body = request.split('\n')[-1]
        comando, resource, resto = request.split(' ', 2)

        return body, comando, resource, resto

    def process(self, parsedRequest):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """

        body = parsedRequest[0]
        comando = parsedRequest[1]
        resourceName = parsedRequest[2]

        if comando == "GET":
            if resourceName in self.content.keys():
                httpCode = "200 OK"
                htmlBody = "<html><body>" + self.content[resourceName] \
                    + "</body></html>"
            else:
                httpCode = "404 Not Found"
                htmlBody = """<html><body>
                <header>Recurso no encontrado, puedes introducir lo que esperabas recibir</header><br><form action="" method = "POST"> 
                <input type="text" name="name" value="">
                <input type="submit" value="Send">
                </form></body></html>
                """
        else:
            self.content[resourceName] = body.split('=')[1]
            print("cuerpo = " + body)
            httpCode = "200 OK"
            htmlBody = "<html><body> El recurso solicitado ha sido: " + resourceName + "<br>" \
            + "<br> Su contenido es: " + self.content[resourceName] + "</body></html>"


        return (httpCode, htmlBody)


if __name__ == "__main__":
    try:
        testContentPostApp = ContentPostApp("localhost", 1234)
    except KeyboardInterrupt:
        print("Exiting...")